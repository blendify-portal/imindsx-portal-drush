diff --git a/includes/image.inc b/includes/image.inc
index e30a338..ce4e51d 100644
--- a/includes/image.inc
+++ b/includes/image.inc
@@ -147,8 +147,7 @@ function image_get_info($filepath, $toolkit = FALSE) {
  * Scales an image to the exact width and height given.
  *
  * This function achieves the target aspect ratio by cropping the original image
- * equally on both sides, or equally on the top and bottom. This function is
- * useful to create uniform sized avatars from larger images.
+ * according to the selected anchor type.
  *
  * The resulting image always has the exact target dimensions.
  *
@@ -158,6 +157,8 @@ function image_get_info($filepath, $toolkit = FALSE) {
  *   The target width, in pixels.
  * @param $height
  *   The target height, in pixels.
+ * @param $anchor
+ *   The anchor to use when cropping.
  *
  * @return
  *   TRUE on success, FALSE on failure.
@@ -165,11 +166,15 @@ function image_get_info($filepath, $toolkit = FALSE) {
  * @see image_load()
  * @see image_resize()
  * @see image_crop()
+ * @see image_scale_and_crop_effect()
  */
-function image_scale_and_crop(stdClass $image, $width, $height) {
+function image_scale_and_crop(stdClass $image, $width, $height, $anchor = 'center-center') {
   $scale = max($width / $image->info['width'], $height / $image->info['height']);
-  $x = ($image->info['width'] * $scale - $width) / 2;
-  $y = ($image->info['height'] * $scale - $height) / 2;
+
+  // Set the top left coordinates of the crop area, based on the anchor.
+  list($x, $y) = explode('-', $anchor);
+  $x = image_filter_keyword($x, $image->info['width'] * $scale, $width);
+  $y = image_filter_keyword($y, $image->info['height'] * $scale, $height);
 
   if (image_resize($image, $image->info['width'] * $scale, $image->info['height'] * $scale)) {
     return image_crop($image, $x, $y, $width, $height);
diff --git a/modules/image/image.effects.inc b/modules/image/image.effects.inc
index 35a6a74..436a72a 100644
--- a/modules/image/image.effects.inc
+++ b/modules/image/image.effects.inc
@@ -28,11 +28,11 @@ function image_image_effect_info() {
     ),
     'image_scale_and_crop' => array(
       'label' => t('Scale and crop'),
-      'help' => t('Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.'),
+      'help' => t('Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension based on the type of anchor selected.'),
       'effect callback' => 'image_scale_and_crop_effect',
       'dimensions callback' => 'image_resize_dimensions',
-      'form callback' => 'image_resize_form',
-      'summary theme' => 'image_resize_summary',
+      'form callback' => 'image_crop_form',
+      'summary theme' => 'image_crop_summary',
     ),
     'image_crop' => array(
       'label' => t('Crop'),
@@ -172,6 +172,7 @@ function image_scale_dimensions(array &$dimensions, array $data) {
  *     "top", "center", "bottom".
  * @return
  *   TRUE on success. FALSE on failure to crop image.
+ *
  * @see image_crop()
  */
 function image_crop_effect(&$image, $data) {
@@ -200,12 +201,21 @@ function image_crop_effect(&$image, $data) {
  *   with the following items:
  *   - "width": An integer representing the desired width in pixels.
  *   - "height": An integer representing the desired height in pixels.
+ *   - "anchor": A string describing where the crop should originate in the form
+ *     of "XOFFSET-YOFFSET". XOFFSET is either a number of pixels or
+ *     "left", "center", "right" and YOFFSET is either a number of pixels or
+ *     "top", "center", "bottom".
  * @return
  *   TRUE on success. FALSE on failure to scale and crop image.
  * @see image_scale_and_crop()
  */
 function image_scale_and_crop_effect(&$image, $data) {
-  if (!image_scale_and_crop($image, $data['width'], $data['height'])) {
+  // Set sane default values.
+  $data += array(
+    'anchor' => 'center-center',
+  );
+
+  if (!image_scale_and_crop($image, $data['width'], $data['height'], $data['anchor'])) {
     watchdog('image', 'Image scale and crop failed using the %toolkit toolkit on %path (%mimetype, %dimensions)', array('%toolkit' => $image->toolkit, '%path' => $image->source, '%mimetype' => $image->info['mime_type'], '%dimensions' => $image->info['width'] . 'x' . $image->info['height']), WATCHDOG_ERROR);
     return FALSE;
   }
diff --git a/modules/simpletest/tests/image.test b/modules/simpletest/tests/image.test
index dc95a6e..e87149e 100644
--- a/modules/simpletest/tests/image.test
+++ b/modules/simpletest/tests/image.test
@@ -144,7 +144,7 @@ class ImageToolkitUnitTest extends ImageToolkitTestCase {
    * Test the image_scale_and_crop() function.
    */
   function testScaleAndCrop() {
-    $this->assertTrue(image_scale_and_crop($this->image, 5, 10), 'Function returned the expected value.');
+    $this->assertTrue(image_scale_and_crop($this->image, 5, 10, 'center-center'), 'Function returned the expected value.');
     $this->assertToolkitOperationsCalled(array('resize', 'crop'));
 
     // Check the parameters.
